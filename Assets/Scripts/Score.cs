﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    Text text;
    int score;
    int highScore;

    // Start is called before the first frame update
    void Start() 
    {
        if (PlayerPrefs.HasKey("Score"))
        {
            highScore = PlayerPrefs.GetInt("Score");
        }

        text = GetComponent<Text>();
        text.text = score.ToString();
    }

    public void UpdateScore (int value)
    {
        score += value;
        text.text = score.ToString();
    }

    public bool SaveScore ()
    {
        PlayerPrefs.SetInt("Score", score);
        if (score > highScore) return true;
        return false;
    }
}
